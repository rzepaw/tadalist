import 'package:flutter/material.dart';
import 'small-todo.dart';
import 'details.dart';

class TasksList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      appBar: AppBar(
        title: const Text('to.do'),
      ),
      body: Container(
          child: ListView(
            children: <Widget>[
              SmallTodo(),
              SmallTodo(),
              SmallTodo(),
            ],
          )
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(
              context,
              Details.routeName,
              arguments: "1Ehg4lRoSUTNzK8z7bQh");
        },
        tooltip: 'Details',
        child: const Icon(Icons.search),
      ),
    );
  }
}
