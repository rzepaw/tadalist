import 'package:firebase/firebase.dart';
import 'package:firebase/firestore.dart' as fs;

class Todo {

  static const String TODOS_PATH = "todos";

  String id;
  String title;
  String description;

  Todo(String id, String title, String description) {
    print("Todo id: $id");
    this.id = id;
    this.title = title;
    this.description = description;
  }

  static Future<Todo> byId(String todoId) async {
    fs.Firestore store = firestore();
    fs.DocumentSnapshot documentSnapshot = await store.doc("$TODOS_PATH/$todoId").get();
    String title = documentSnapshot.get('title');
    String description = documentSnapshot.get('description');
    return Todo(todoId, title, description);
  }
}
