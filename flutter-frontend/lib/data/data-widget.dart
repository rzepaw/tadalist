import 'package:flutter/material.dart';

abstract class DataWidget<T> extends StatelessWidget {

  final Future<T> future;
  final Widget child;

  const DataWidget({
    this.future,
    this.child
  }) : assert(future != null),
        assert(child != null);

  Widget build(BuildContext context) {
    return FutureBuilder<T>(
        future: this.future,
        builder: (BuildContext context, AsyncSnapshot<T> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('Press button to start.');
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Text('Awaiting result...');
            case ConnectionState.done:
              if (snapshot.hasError)
                return Text('Error: ${snapshot.error}');
              return this.child;
          }
          return null; // unreachable
        }
    );
  }
}
