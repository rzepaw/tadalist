import 'package:flutter/material.dart';
import 'list.dart';
import 'details.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      initialRoute: '/',
      routes: {
        '/': (context) => TasksList(),
        Details.routeName: (context) => Details(),
      },
      theme: ThemeData.dark()
    );
  }
}
