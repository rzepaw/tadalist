import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'data/toto.dart';

class SmallTodo extends StatelessWidget {

  void valueChanged(bool value) => {

  };

  Widget checkbox(String todoId) {
    return Checkbox(
        value: false,
        onChanged: valueChanged
    );
  }

  Widget textInput(String todoId) {
    return FutureBuilder<Todo>(
        future: Todo.byId(todoId),
        builder: (BuildContext context, AsyncSnapshot<Todo> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('Press button to start.');
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Text('Awaiting result...');
            case ConnectionState.done:
              if (snapshot.hasError)
                return Text('Error: ${snapshot.error}');
              // return Text(snapshot.data.get('title'));
              return TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: snapshot.data.title,
                ),
              );
          }
          return null; // unreachable
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10.0),
        child: Row (
          children: <Widget>[
            checkbox("asdf"),
            Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                        children: <Widget>[
                          Text("Text input"),
                          Expanded(
                            child: textInput("1Ehg4lRoSUTNzK8z7bQh")
                          )
                        ]
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: DatePickerTimeline(
                            DateTime.now(),
                            onDateChange: (date) {
                              print(date.day.toString());
                            },
                          ),
                        )
                      ],
                    )
                  ],
                )
            )
          ],
        )
    );
  }
}
