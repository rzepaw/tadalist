import 'package:flutter/material.dart';
import 'details/history.dart';
import 'details/description.dart';
import 'details/attachments.dart';
import 'details/links.dart';
import 'details/deadline.dart';
import 'details/involved.dart';

class Details extends StatelessWidget {

  static const routeName = '/this';

  @override
  Widget build(BuildContext context) {

    final String id = ModalRoute.of(context).settings.arguments;

    return Scaffold (
      appBar: AppBar(
        title: const Text('Todo details'),
        actions: <Widget>[
          Text("1Ehg4lRoSUTNzK8z7bQh"),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () { },
          ),
          IconButton(
            icon: Icon(Icons.print),
            onPressed: () { },
          ),
        ],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded (
              flex: 5,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 5,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Description(),
                        ),
                        Expanded(
                          flex: 1,
                          child: Links(),
                        ),
                        Expanded(
                          flex: 1,
                          child: Attachments(),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Involved(),
                        ),
                        Expanded(
                          flex: 3,
                          child: History(),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Deadline(),
            )
          ],
        )
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pop(context);
        },
        tooltip: 'Back',
        child: const Icon(Icons.arrow_back),
      ),
    );
  }
}
