# Tadalist

## Install Flutter

- Windows: https://flutter.dev/docs/get-started/install/windows
- MacOS: https://flutter.dev/docs/get-started/install/macos
- Linux: https://flutter.dev/docs/get-started/install/linux

## Run

```
> flutter pub get
> flutter build web
> flutter run -d chrome
```

## Domain

i.have.to.do