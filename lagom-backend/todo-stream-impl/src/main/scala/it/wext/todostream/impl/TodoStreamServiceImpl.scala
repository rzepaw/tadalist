package it.wext.todostream.impl

import com.lightbend.lagom.scaladsl.api.ServiceCall
import it.wext.todostream.api.TodoStreamService
import it.wext.todo.api.TodoService

import scala.concurrent.Future

/**
  * Implementation of the TodoStreamService.
  */
class TodoStreamServiceImpl(todoService: TodoService) extends TodoStreamService {
  def stream = ServiceCall { hellos =>
    Future.successful(hellos.mapAsync(8)(todoService.hello(_).invoke()))
  }
}
