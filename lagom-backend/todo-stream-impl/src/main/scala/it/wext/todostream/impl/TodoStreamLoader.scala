package it.wext.todostream.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import it.wext.todostream.api.TodoStreamService
import it.wext.todo.api.TodoService
import com.softwaremill.macwire._

class TodoStreamLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new TodoStreamApplication(context) {
      override def serviceLocator: NoServiceLocator.type = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new TodoStreamApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[TodoStreamService])
}

abstract class TodoStreamApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[TodoStreamService](wire[TodoStreamServiceImpl])

  // Bind the TodoService client
  lazy val todoService: TodoService = serviceClient.implement[TodoService]
}
